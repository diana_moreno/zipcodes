#Reto Técnico#

INSTRUCCIONES
El reto consiste en utilizar el framework Laravel para replicar la funcionalidad de este API (Utilities - Zip code) utilizando esta fuente de información. El tiempo de respuesta promedio deberá ser menor a 300 ms, pero entre menor sea, mejor.

 
### Fuente de datos ###

Se agrego el archivo CPdescarga.xls proporcionado por la pagina, para insertar los codigo se implemento Maatwebsite\Excel\Facades\Excel para leer el excel e insertarlo, se agregaron imports separados para que lo soportara y no excediera el limite de tiempo de ejecucion. Este proceso se ejecuta mediante consola con php artisan  zip:codes. 

### Api ###

* Para optimizar el tiempo determine hacer la consulta con sql, pues con 
query builder nos dio un tiempo mayor y ni se diga por eloquent.

