<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Http\Controllers\UploadZipCodesController;

class Zip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zip:codes';

    /**
     * The console Insert zip codes.
     *
     * @var string
     */
    protected $description = 'Insert zip codes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UploadZipCodesController $UploadZipCodesController)
    {
        parent::__construct();
        $this->UploadZipCodesController = $UploadZipCodesController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        ini_set('memory_limit', '2048M');

        \Log::info('Started at:' . Carbon::now() . '.');
        \Log::info('************************************************************');
        $this->UploadZipCodesController->importAguascalientes();
        $this->UploadZipCodesController->importBaja();
        $this->UploadZipCodesController->importBajaSur();
        $this->UploadZipCodesController->importCampeche();
        $this->UploadZipCodesController->importCoahuila();
        $this->UploadZipCodesController->importColima();
        $this->UploadZipCodesController->importChiapas();
        $this->UploadZipCodesController->importChihuahua();
        $this->UploadZipCodesController->importCdmx();
        $this->UploadZipCodesController->importDurango();
        $this->UploadZipCodesController->importGuanajuato();
        $this->UploadZipCodesController->importGuerrero();
        $this->UploadZipCodesController->importHidalgo();
        $this->UploadZipCodesController->importJalisco();
        $this->UploadZipCodesController->importMexico();
        $this->UploadZipCodesController->importMichoacan();
        $this->UploadZipCodesController->importMorelos();
        $this->UploadZipCodesController->importNayarit();
        $this->UploadZipCodesController->importNuevoLeon();
        $this->UploadZipCodesController->importOaxaca();
        $this->UploadZipCodesController->importPuebla();
        $this->UploadZipCodesController->importQueretaro();
        $this->UploadZipCodesController->importQuintanaRoo();
        $this->UploadZipCodesController->importSanLuisPotosi();
        $this->UploadZipCodesController->importSinaloa();
        $this->UploadZipCodesController->importSonora();
        $this->UploadZipCodesController->importTabasco();
        $this->UploadZipCodesController->importTamaulipas();
        $this->UploadZipCodesController->importTlaxcala();
        $this->UploadZipCodesController->importVeracruz();
        $this->UploadZipCodesController->importYucatan();
        $this->UploadZipCodesController->importZacatecas();
        echo "Termino";
        \Log::info('************************************************************');
        \Log::info('Concluded at:' . Carbon::now() . '.');
    }
}
