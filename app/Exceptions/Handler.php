<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                case 404:
                    return response()->json(['message' => 'Invalid request or url.'], 404);
                    break;
                case '500':
                    return response()->json(['message' => 'Server error. Please contact admin.'], 500);
                    break;
                case 429:
                    return response()->json([
                        'result' => false,
                        'message' => 'Too many requests, please retry in 49 seconds',
                        "errors"=> ["TooManyRequests"],
                        "retry_after"=> 49,
                        "status"=> 429
                    ], 500);
                    break;
                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        } else if ($exception instanceof ModelNotFoundException) {
            if (request()->expectsJson()) {
                return response()->json(['message' => $exception->getMessage()], 404);
            }
        } {
            return parent::render($request, $exception);
        }
        return parent::render($request, $exception);
    }
}
