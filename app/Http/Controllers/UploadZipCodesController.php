<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\ZipCodesImport;
use App\Imports\ZipCodesImportBaja;
use App\Imports\ZipCodesImportBajaSur;
use App\Imports\ZipCodesImportCampeche;
use App\Imports\ZipCodesImportCoahuila;
use App\Imports\ZipCodesImportColima;
use App\Imports\ZipCodesImportChiapas;
use App\Imports\ZipCodesImportChihuahua;
use App\Imports\ZipCodesImportCdmx;
use App\Imports\ZipCodesImportDurango;
use App\Imports\ZipCodesImportGuanajuato;
use App\Imports\ZipCodesImportGuerrero;
use App\Imports\ZipCodesImportHidalgo;
use App\Imports\ZipCodesImportJalisco;
use App\Imports\ZipCodesImportMexico;
use App\Imports\ZipCodesImportMichoacan;
use App\Imports\ZipCodesImportMorelos;
use App\Imports\ZipCodesImportNayarit;
use App\Imports\ZipCodesImportNuevoLeon;
use App\Imports\ZipCodesImportOaxaca;
use App\Imports\ZipCodesImportPuebla;
use App\Imports\ZipCodesImportQueretaro;
use App\Imports\ZipCodesImportQuintanaRoo;
use App\Imports\ZipCodesImportSanLuisPotosi;
use App\Imports\ZipCodesImportSinaloa;
use App\Imports\ZipCodesImportSonora;
use App\Imports\ZipCodesImportTabasco;
use App\Imports\ZipCodesImportTamaulipas;
use App\Imports\ZipCodesImportTlaxcala;
use App\Imports\ZipCodesImportVeracruz;
use App\Imports\ZipCodesImportYucatan;
use App\Imports\ZipCodesImportZacatecas;

class UploadZipcodesController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
        $this->excelXls = 'CPdescarga.xls';
    }

    public function importAguascalientes()
    {
        Excel::import(new ZipCodesImport, $this->excelXls);
    } 
    public function importBaja()
    {
        Excel::import(new ZipCodesImportBaja, $this->excelXls);
    }
    public function importBajaSur()
    {
        Excel::import(new ZipCodesImportBajaSur, $this->excelXls);
    }

    public function importCampeche()
    {
        Excel::import(new ZipCodesImportCampeche, $this->excelXls);
    }
    public function importCoahuila()
    {
        Excel::import(new ZipCodesImportCoahuila, $this->excelXls);
    }
    public function importColima()
    {
        Excel::import(new ZipCodesImportColima, $this->excelXls);
    }
    public function importChiapas()
    {
        Excel::import(new ZipCodesImportChiapas, $this->excelXls);
    }
    public function importChihuahua()
    {
        Excel::import(new ZipCodesImportChihuahua, $this->excelXls);
    }
    public function importCdmx()
    {
        Excel::import(new ZipCodesImportCdmx, $this->excelXls);
    }
    public function importDurango()
    {
        Excel::import(new ZipCodesImportDurango, $this->excelXls);
    }
    public function importGuanajuato()
    {
        Excel::import(new ZipCodesImportGuanajuato, $this->excelXls);
    }
    public function importGuerrero()
    {
        Excel::import(new ZipCodesImportGuerrero, $this->excelXls);
    }
    public function importHidalgo()
    {
        Excel::import(new ZipCodesImportHidalgo, $this->excelXls);
    }
    public function importJalisco()
    {
        Excel::import(new ZipCodesImportJalisco, $this->excelXls);
    }
    public function importMexico()
    {
        Excel::import(new ZipCodesImportMexico, $this->excelXls);
    }
    public function importMichoacan()
    {
        Excel::import(new ZipCodesImportMichoacan, $this->excelXls);
    }
    public function importMorelos()
    {
        Excel::import(new ZipCodesImportMorelos, $this->excelXls);
    }
    public function importNayarit()
    {
        Excel::import(new ZipCodesImportNayarit, $this->excelXls);
    }
    public function importNuevoLeon()
    {
        Excel::import(new ZipCodesImportNuevoLeon, $this->excelXls);
    }
    public function importOaxaca()
    {
        Excel::import(new ZipCodesImportOaxaca, $this->excelXls);
    }
    public function importPuebla()
    {
        Excel::import(new ZipCodesImportPuebla, $this->excelXls);
    }
    public function importQueretaro()
    {
        Excel::import(new ZipCodesImportQueretaro, $this->excelXls);
    }
    public function importQuintanaRoo()
    {
        Excel::import(new ZipCodesImportQuintanaRoo, $this->excelXls);
    }
    public function importSanLuisPotosi()
    {
        Excel::import(new ZipCodesImportSanLuisPotosi, $this->excelXls);
    }
    public function importSinaloa()
    {
        Excel::import(new ZipCodesImportSinaloa, $this->excelXls);
    }
    public function importSonora()
    {
        Excel::import(new ZipCodesImportSonora, $this->excelXls);
    }
    public function importTabasco()
    {
        Excel::import(new ZipCodesImportTabasco, $this->excelXls);
    }
    public function importTamaulipas()
    {
        Excel::import(new ZipCodesImportTamaulipas, $this->excelXls);
    }
    public function importTlaxcala()
    {
        Excel::import(new ZipCodesImportTlaxcala, $this->excelXls);
    }
    public function importVeracruz()
    {
        Excel::import(new ZipCodesImportVeracruz, $this->excelXls);
    }
    public function importYucatan()
    {
        Excel::import(new ZipCodesImportYucatan, $this->excelXls);
    }
    public function importZacatecas()
    {
        Excel::import(new ZipCodesImportZacatecas, $this->excelXls);
    }
}
