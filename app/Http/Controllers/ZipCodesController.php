<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ZipCodes;
use Illuminate\Support\Arr;
use DB;

class ZipCodesController extends Controller
{
    public function getZipCode($zipcode)
    {
       $codes = DB::select('select d_codigo,d_ciudad,d_estado,d_asenta,d_zona,d_tipo_asenta,d_mnpio from zipcodes where d_codigo ="'. $zipcode.'"');
        if (!empty($codes) && count($codes) >= 1) {
            $municipality = $settlements = $zipCodeResponse = array();
            foreach ($codes as $code) {
                $zipCodeResponse =  array(
                    "zip_code" => $code->d_codigo,
                    "locality" => $code->d_ciudad,
                    "federal_entity" => array(
                        "name" => $code->d_estado,
                        "code" => $code->d_estado
                    ),
                );
                $settlements[] =
                    array(
                        "name" => $code->d_asenta,
                        "zone_type" => $code->d_zona,
                        "settlement_type" => array("name" => $code->d_tipo_asenta)
                    );
                $municipality = array(
                    "name" => $code->d_mnpio
                );
            }
            $zipCodeResponse['settlements'] = $settlements;
            $zipCodeResponse['municipality'] = $municipality;
        } else {
            $zipCodeResponse =  array(
                "status" => false,
                "message" => "Código no existente"
            );
        }

        return response()->json($zipCodeResponse);
    }
}
