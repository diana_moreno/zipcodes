<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportBaja implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Baja_California'=> new FirstSheetImport(),
        ];
    }
     
}
