<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportBajaSur implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Baja_California_Sur'=> new FirstSheetImport(),
        ];
    }
     
}
