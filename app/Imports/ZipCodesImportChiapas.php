<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportChiapas implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Chiapas'=> new FirstSheetImport(),
        ];
    }
     
}
