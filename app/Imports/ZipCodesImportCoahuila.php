<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportCoahuila implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Coahuila_de_Zaragoza'=> new FirstSheetImport(),
        ];
    }
     
}
