<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportColima implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Colima'=> new FirstSheetImport(),
        ];
    }
     
}
