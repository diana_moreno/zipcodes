<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportGuerrero implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Guerrero'=> new FirstSheetImport(),
        ];
    }
     
}
