<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportMichoacan implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Michoacán_de_Ocampo'=> new FirstSheetImport(),
        ];
    }
     
}
