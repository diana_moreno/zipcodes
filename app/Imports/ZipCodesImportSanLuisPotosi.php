<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportSanLuisPotosi implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'San_Luis_Potosí'=> new FirstSheetImport(),
        ];
    }
     
}
