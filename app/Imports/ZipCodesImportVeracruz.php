<?php

namespace App\Imports;

use App\Models\ZipCodes;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ZipCodesImportVeracruz implements WithMultipleSheets
{
    use Importable;
    public function sheets(): array
    {
        return [
            'Veracruz_de_Ignacio_de_la_Llave'=> new FirstSheetImport(),
        ];
    }
     
}
