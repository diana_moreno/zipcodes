<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ZipCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zipcodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('d_codigo',5)->index();
            $table->string('d_asenta');
            $table->string('d_tipo_asenta');
            $table->string('d_mnpio')->nullable();
            $table->string('d_estado');
            $table->string('d_ciudad')->nullable();
            $table->string('d_cp')->nullable();
            $table->string('c_estado');
            $table->string('c_oficina');
            $table->string('c_cp')->nullable();
            $table->string('c_tipo_asenta');
            $table->string('c_mnpio');
            $table->string('id_asenta_cpcons');
            $table->string('d_zona');
            $table->string('c_cve_ciudad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
